﻿using Proiect_TMW.Services;

namespace Proiect_TMW
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();
        }
    }
}

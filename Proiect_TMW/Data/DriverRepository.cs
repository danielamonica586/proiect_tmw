﻿using Proiect_TMW.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Data
{
    public class DriverRepository : IDriverRepository
    {
        private SQLiteAsyncConnection connection;

        private async Task Initialize()
        {
            if (connection == null)
            {
                connection = new SQLiteAsyncConnection(
                    Path.Combine(FileSystem.AppDataDirectory, Utils.Constants.DatabaseFile),
                    SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.SharedCache);
                await connection.CreateTableAsync<Driver>();
            }
        }

        public async Task<IList<Driver>> GetDriversAsync()
        {
            await Initialize();
            return await connection.Table<Driver>().ToListAsync();
        }

        public async Task SaveDriversAsync(IList<Driver> drivers)
        {
            await Initialize();
            await connection.DeleteAllAsync<Driver>();
            await connection.InsertAllAsync(drivers);
        }

    }
}

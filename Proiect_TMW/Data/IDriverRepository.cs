﻿using Proiect_TMW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Data
{
    public interface IDriverRepository
    {
        Task<IList<Driver>> GetDriversAsync();
        Task SaveDriversAsync(IList<Driver> drivers);

    }
}

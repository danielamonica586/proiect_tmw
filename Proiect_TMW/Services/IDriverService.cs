﻿using Proiect_TMW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Services
{
    public interface IDriverService
    {
        Task<IList<Driver>> GetAllDriversAsync();
    }
}

﻿using Newtonsoft.Json.Linq;
using Proiect_TMW.Data;
using Proiect_TMW.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Proiect_TMW.Services
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _repository;
        private const int PageSize = 30;

        public DriverService(IDriverRepository repository)
        {
            _repository = repository;
        }

        public async Task<IList<Driver>> GetAllDriversAsync()
        {
            var drivers = new List<Driver>();
            bool internetAvailable = true;

            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(30);

                int offset = 0;
                bool moreDataAvailable = true;

                while (moreDataAvailable)
                {
                    try
                    {
                        var response = await client.GetStringAsync($"{Utils.Constants.DriverEndpoint}?limit={PageSize}&offset={offset}");
                        var data = JObject.Parse(response);

                        foreach (var item in data["MRData"]["DriverTable"]["Drivers"])
                        {
                            var driverObject = item as JObject;

                            var permanentNumber = driverObject != null && driverObject.ContainsKey("permanentNumber") && !string.IsNullOrWhiteSpace(driverObject["permanentNumber"].ToString())
                                                 ? driverObject["permanentNumber"].ToString()
                                                 : "-";
                            var givenName = item["givenName"]?.ToString() ?? string.Empty;
                            var familyName = item["familyName"]?.ToString() ?? string.Empty;
                            var dateOfBirth = item["dateOfBirth"] != null ? DateTime.Parse(item["dateOfBirth"].ToString()) : DateTime.MinValue;
                            var nationality = item["nationality"]?.ToString() ?? string.Empty;

                            var driver = new Driver
                            {
                                PermanentNumber = permanentNumber,
                                GivenName = givenName,
                                FamilyName = familyName,
                                DateOfBirth = dateOfBirth,
                                Nationality = nationality
                            };

                            drivers.Add(driver);
                        }

                        int total = int.Parse(data["MRData"]["total"].ToString());
                        offset += PageSize;
                        moreDataAvailable = offset < total;
                    }
                    catch (HttpRequestException e)
                    {
                        System.Diagnostics.Debug.WriteLine($"Request error: {e.Message}");
                        internetAvailable = false;
                        moreDataAvailable = false;
                    }
                    catch (TaskCanceledException e)
                    {
                        System.Diagnostics.Debug.WriteLine($"Request timed out: {e.Message}");
                        internetAvailable = false;
                        moreDataAvailable = false;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine($"Unexpected exception: {e.Message}");
                        internetAvailable = false;
                        moreDataAvailable = false;
                    }
                }

                if (internetAvailable)
                {
                    await _repository.SaveDriversAsync(drivers);
                }
                else
                {
                    drivers = (List<Driver>)await _repository.GetDriversAsync();
                }

                return drivers;
            }
        }
    }
}

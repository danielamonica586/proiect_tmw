﻿using Proiect_TMW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Services
{
    public class MenuService
    {
        public List<MenuOption> GetMenuOptions()
        {
            return new List<MenuOption>
            {
                new MenuOption { Title = "Season List", Endpoint = "seasons" },
                new MenuOption { Title = "Race Schedule", Endpoint = "races" },
                new MenuOption { Title = "Drivers", Endpoint = "drivers" },
                new MenuOption { Title = "Constructors", Endpoint = "constructors" },
                new MenuOption { Title = "Circuits", Endpoint = "circuits" }
            };
        }
    }
}

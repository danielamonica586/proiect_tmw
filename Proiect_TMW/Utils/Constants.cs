﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Utils
{
    public static class Constants
    {
        public const string DatabaseFile = "Drivers.db";
        public const string DriverEndpoint = "https://ergast.com/api/f1/drivers.json";
    }
}

﻿using Proiect_TMW.Model;
using Proiect_TMW.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.ViewModel
{
    public class MenuViewModel
    {
        public System.Collections.ObjectModel.ObservableCollection<MenuOption> MenuOptions { get; set; }

        public MenuViewModel()
        {
            var menuService = new MenuService();
            MenuOptions = new System.Collections.ObjectModel.ObservableCollection<MenuOption>(menuService.GetMenuOptions());
        }
    }
}

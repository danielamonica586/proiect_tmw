﻿using Proiect_TMW.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Proiect_TMW.ViewModel
{
    public interface IDriverViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Driver> Drivers { get; }
        ObservableCollection<string> SortOptions { get; }
        ObservableCollection<string> Nationalities { get; }

        string SelectedSortOption { get; set; }
        string SearchText { get; set; }
        string SelectedNationality { get; set; }
        string FilterStartYear { get; set; }
        string FilterEndYear { get; set; }

        bool IsBusy { get; }
        bool NoResultsFound { get; }

        ICommand LoadFirstPage { get; }
        ICommand LoadNextPageCommand { get; }
        ICommand LoadPreviousPageCommand { get; }
        ICommand SearchCommand { get; }
        ICommand ApplyFiltersCommand { get; }
        ICommand ResetFiltersCommand { get; }

        Task LoadDrivers();

        void SortDrivers(string option);
        void SearchDrivers();
        void ApplyFilters();
        void ResetFilters();

        //Task ResetAndLoadDrivers();
    }
}

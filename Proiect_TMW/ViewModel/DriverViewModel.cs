﻿using Proiect_TMW.Data;
using Proiect_TMW.Model;
using Proiect_TMW.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Proiect_TMW.ViewModel
{
    public class DriverViewModel : INotifyPropertyChanged, IDriverViewModel
    {
        private const int PageSize = 30;
        private int currentPage;

        private readonly IDriverService driverService;
        private readonly IDriverRepository driverRepository;

        private IList<Driver> allDrivers;
        private IList<Driver> currentDrivers;
        private IList<Driver> searchResults;
        private IList<Driver> filteredDrivers;

        private string permanentNumber;
        private string givenName;
        private string familyName;
        private string dateOfBirth;
        private string nationality;

        private bool isBusy;
        private bool noResultsFound;
        private bool isSearching;

        private string selectedSortOption;
        private string searchText;
        private string selectedNationality;
        private string filterStartYear;
        private string filterEndYear;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Driver> Drivers { get; private set; }
        public ObservableCollection<string> SortOptions { get; private set; }
        public ObservableCollection<string> Nationalities { get; private set; }

        public ICommand LoadFirstPage { get; private set; }
        public ICommand LoadNextPageCommand { get; private set; }
        public ICommand LoadPreviousPageCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }
        public ICommand ApplyFiltersCommand { get; private set; }
        public ICommand ResetFiltersCommand { get; private set; }

        public bool ResultsFound => Drivers.Count > 0;

        public DriverViewModel(IDriverService driverService, IDriverRepository driverRepository)
        {
            this.driverService = driverService;
            this.driverRepository = driverRepository;
            Drivers = new ObservableCollection<Driver>();
            SortOptions = new ObservableCollection<string>
            {
                "Ascending, family name",
                "Ascending, given name",
                "Descending, family name",
                "Descending, given name"
            };
            Nationalities = new ObservableCollection<string>
            {
                "All",
                "American", "Argentine", "Argentine-Italian", "Austrian", "Australian", "Belgian",
                "Brazilian", "British", "Canadian", "Chilean", "Chinese", "Colombian",
                "Dutch", "East German", "Finnish", "French", "German", "Hungarian",
                "Indian", "Indonesian", "Irish", "Italian", "Japanese", "Lichtensteiner",
                "Malaysian", "Mexican", "Monegasque", "New Zealander", "Polish",
                "Portuguese", "Rhodesian", "Russian", "South African", "Spanish",
                "Swedish", "Swiss", "Thai", "Uruguayan", "Venezuelan"
            };
            LoadFirstPage = new Command(async () => await LoadFirstPageAsync());
            LoadNextPageCommand = new Command(async () => await LoadNextPage());
            LoadPreviousPageCommand = new Command(async () => await LoadPreviousPage());
            SearchCommand = new Command(SearchDrivers);
            ApplyFiltersCommand = new Command(ApplyFilters);
            ResetFiltersCommand = new Command(ResetFilters);

            currentPage = 1;
            isSearching = false;

            // Load the first page of drivers when the ViewModel is initialized
            LoadFirstPage.Execute(null);
        }

        public string PermanentNumber
        {
            get => permanentNumber;
            set
            {
                permanentNumber = value;
                OnPropertyChanged(nameof(PermanentNumber));
            }
        }

        public string GivenName
        {
            get => givenName;
            set
            {
                givenName = value;
                OnPropertyChanged(nameof(GivenName));
            }
        }

        public string FamilyName
        {
            get => familyName;
            set
            {
                familyName = value;
                OnPropertyChanged(nameof(FamilyName));
            }
        }

        public string DateOfBirth
        {
            get => dateOfBirth;
            set
            {
                dateOfBirth = value;
                OnPropertyChanged(nameof(DateOfBirth));
            }
        }

        public string Nationality
        {
            get => nationality;
            set
            {
                nationality = value;
                OnPropertyChanged(nameof(Nationality));
            }
        }


        public bool IsBusy
        {
            get => isBusy;
            set
            {
                isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
                Debug.WriteLine($"IsBusy set to: {isBusy}");
                ((Command)LoadFirstPage).ChangeCanExecute();
                ((Command)LoadNextPageCommand).ChangeCanExecute();
                ((Command)LoadPreviousPageCommand).ChangeCanExecute();
                ((Command)SearchCommand).ChangeCanExecute();
                ((Command)ApplyFiltersCommand).ChangeCanExecute();
                ((Command)ResetFiltersCommand).ChangeCanExecute();
            }
        }

        public string SelectedSortOption
        {
            get => selectedSortOption;
            set
            {
                selectedSortOption = value;
                OnPropertyChanged(nameof(SelectedSortOption));
                if (!string.IsNullOrEmpty(selectedSortOption))
                {
                    SortDrivers(selectedSortOption);
                }
            }
        }

        public string SearchText
        {
            get => searchText;
            set
            {
                searchText = value;
                OnPropertyChanged(nameof(SearchText));
            }
        }

        public string SelectedNationality
        {
            get => selectedNationality;
            set
            {
                selectedNationality = value;
                OnPropertyChanged(nameof(SelectedNationality));
            }
        }

        public string FilterStartYear
        {
            get => filterStartYear;
            set
            {
                filterStartYear = value;
                OnPropertyChanged(nameof(FilterStartYear));
            }
        }

        public string FilterEndYear
        {
            get => filterEndYear;
            set
            {
                filterEndYear = value;
                OnPropertyChanged(nameof(FilterEndYear));
            }
        }

        public bool NoResultsFound
        {
            get => noResultsFound;
            set
            {
                noResultsFound = value;
                OnPropertyChanged(nameof(NoResultsFound));
            }
        }

        public async Task LoadDrivers()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            Debug.WriteLine("Loading drivers...");

            try
            {
                // Load all drivers from the API
                allDrivers = await driverService.GetAllDriversAsync();
                Debug.WriteLine($"Total drivers loaded from API: {allDrivers.Count}");

                // Save drivers to the local database
                await driverRepository.SaveDriversAsync(allDrivers);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error loading drivers from API: {ex.Message}");

                // Load drivers from the database if the API fails
                allDrivers = await driverRepository.GetDriversAsync();
                Debug.WriteLine($"Total drivers loaded from local database: {allDrivers.Count}");
            }

            currentDrivers = new List<Driver>(allDrivers); // Initially, currentDrivers holds all drivers
            searchResults = new List<Driver>(allDrivers); // Initially, searchResults holds all drivers
            filteredDrivers = new List<Driver>(allDrivers);

            if (!string.IsNullOrEmpty(selectedSortOption))
            {
                SortDrivers(selectedSortOption);
            }

            DisplayCurrentPage();
            IsBusy = false;
        }

        private async Task LoadFirstPageAsync()
        {
            currentPage = 1;
            await LoadDrivers();
        }

        private void DisplayCurrentPage()
        {
            Drivers.Clear();
            Debug.WriteLine($"Displaying page {currentPage}");

            int startIndex = (currentPage - 1) * PageSize;
            int endIndex = Math.Min(startIndex + PageSize, currentDrivers.Count);

            for (int i = startIndex; i < endIndex; i++)
            {
                Drivers.Add(currentDrivers[i]);
            }

            NoResultsFound = Drivers.Count == 0;
            OnPropertyChanged(nameof(ResultsFound));
            OnPropertyChanged(nameof(NoResultsFound));
            OnPropertyChanged(nameof(SearchText));
            Debug.WriteLine($"Page {currentPage} displayed with {Drivers.Count} drivers.");
        }

        private async Task LoadNextPage()
        {
            if (IsBusy || (currentPage * PageSize) >= currentDrivers.Count)
                return;

            //simulate loading (delay)
            IsBusy = true;
            Debug.WriteLine("Loading next page");
            await Task.Delay(500);
            currentPage++;
            DisplayCurrentPage();
            IsBusy = false;
        }

        private async Task LoadPreviousPage()
        {
            if (IsBusy || currentPage <= 1)
                return;

            //simulate loading (delay)
            IsBusy = true;
            Debug.WriteLine("Loading previous page");
            await Task.Delay(500);
            currentPage--;
            DisplayCurrentPage();
            IsBusy = false;
        }


        public void SortDrivers(string option)
        {
            if (string.IsNullOrEmpty(option))
                return;

            Debug.WriteLine($"Sorting by: {option}");

            string field = string.Empty;
            bool ascending = true;

            switch (option)
            {
                case "Ascending, family name":
                    field = nameof(Driver.FamilyName);
                    ascending = true;
                    break;
                case "Ascending, given name":
                    field = nameof(Driver.GivenName);
                    ascending = true;
                    break;
                case "Descending, family name":
                    field = nameof(Driver.FamilyName);
                    ascending = false;
                    break;
                case "Descending, given name":
                    field = nameof(Driver.GivenName);
                    ascending = false;
                    break;
            }

            try
            {
                if (ascending)
                {
                    currentDrivers = currentDrivers.OrderBy(d => d.GetType().GetProperty(field).GetValue(d)).ToList();
                }
                else
                {
                    currentDrivers = currentDrivers.OrderByDescending(d => d.GetType().GetProperty(field).GetValue(d)).ToList();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error during sorting: {ex.Message}");
            }

            currentPage = 1;
            DisplayCurrentPage();
        }

        public void SearchDrivers()
        {
            Debug.WriteLine($"Searching for: {SearchText}");

            try
            {
                if (string.IsNullOrEmpty(SearchText))
                {
                    searchResults = new List<Driver>(allDrivers); // Reset to all drivers if search text is empty
                }
                else
                {
                    searchResults = allDrivers.Where(d =>
                        d.GivenName.Contains(SearchText, StringComparison.OrdinalIgnoreCase) ||
                        d.FamilyName.Contains(SearchText, StringComparison.OrdinalIgnoreCase) ||
                        (d.GivenName + " " + d.FamilyName).Contains(SearchText, StringComparison.OrdinalIgnoreCase)).ToList();
                }

                if (searchResults.Count == 0)
                {
                    // Reset the sort option if no results found
                    selectedSortOption = null;
                    OnPropertyChanged(nameof(SelectedSortOption));
                }

                currentDrivers = new List<Driver>(searchResults);

                if (!string.IsNullOrEmpty(selectedSortOption))
                {
                    SortDrivers(selectedSortOption);
                }
                else
                {
                    currentPage = 1;
                    DisplayCurrentPage();

                    SearchText = string.Empty;
                    OnPropertyChanged(nameof(SearchText));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error during search: {ex.Message}");
            }
        }

        public void ApplyFilters()
        {
            var filteredList = searchResults.AsEnumerable();

            if (!string.IsNullOrEmpty(SelectedNationality) && SelectedNationality != "All")
            {
                filteredList = filteredList.Where(d => d.Nationality == SelectedNationality);
            }

            if (int.TryParse(FilterStartYear, out int startYear))
            {
                filteredList = filteredList.Where(d => d.DateOfBirth.Year >= startYear);
            }

            if (int.TryParse(FilterEndYear, out int endYear))
            {
                filteredList = filteredList.Where(d => d.DateOfBirth.Year <= endYear);
            }

            currentDrivers = filteredList.ToList();

            if (!string.IsNullOrEmpty(selectedSortOption))
            {
                SortDrivers(selectedSortOption);
            }

            currentPage = 1;
            DisplayCurrentPage();

            NoResultsFound = Drivers.Count == 0;
            OnPropertyChanged(nameof(NoResultsFound));
        }

        public void ResetFilters()
        {
            SelectedNationality = "All";
            FilterStartYear = string.Empty;
            FilterEndYear = string.Empty;
            SearchText = string.Empty;
            searchResults = new List<Driver>(allDrivers);
            currentDrivers = new List<Driver>(allDrivers);

            if (!string.IsNullOrEmpty(selectedSortOption))
            {
                SortDrivers(selectedSortOption);
            }

            currentPage = 1;
            DisplayCurrentPage();
        }


        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            Debug.WriteLine($"Property changed: {propertyName}");
        }
    }
}

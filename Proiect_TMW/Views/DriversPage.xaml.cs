using Proiect_TMW.ViewModel;

namespace Proiect_TMW.Views;

public partial class DriversPage : ContentPage
{
    private readonly IDriverViewModel viewModel;
    public DriversPage(IDriverViewModel viewModel)
	{
		InitializeComponent();
        BindingContext = viewModel;
        this.viewModel = viewModel;
    }

    //protected override void OnAppearing()
    //{
    //    base.OnAppearing();
    //    viewModel?.ResetAndLoadDrivers().ConfigureAwait(false);
    //}
}
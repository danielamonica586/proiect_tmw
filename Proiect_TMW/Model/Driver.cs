﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Proiect_TMW.Model
{
    public class Driver
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string PermanentNumber { get; set; } 
        public DateTime DateOfBirth { get; set; }
        public string Nationality { get; set; }
    }
}

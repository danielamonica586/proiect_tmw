﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_TMW.Model
{
    public class MenuOption
    {
        public string Title { get; set; }
        public string Endpoint { get; set; }
    }
}

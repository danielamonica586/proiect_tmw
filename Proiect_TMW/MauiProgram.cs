﻿using Microsoft.Extensions.Logging;
using Proiect_TMW.Data;
using Proiect_TMW.Services;
using Proiect_TMW.ViewModel;
using Proiect_TMW.Views;

namespace Proiect_TMW
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
            builder.Logging.AddDebug();
#endif
            //Configurarea bazei de date
            builder.Services.AddSingleton<Data.IDriverRepository, Data.DriverRepository>();
            
            // Configurarea serviciilor
            builder.Services.AddSingleton<IDriverService, DriverService>();

            // Configurarea ViewModel-urilor
            builder.Services.AddTransient<IDriverViewModel, DriverViewModel>();

            // Configurarea paginilor
            builder.Services.AddTransient<DriversPage>();

            return builder.Build();
        }
    }
}
